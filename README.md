### Simulando serviços AWS na sua máquina com LocalStack   


## Docker-compose.yaml   
```
localstack:
    image: localstack/localstack
    container_name: localstack
    ports:
      - "4566:4566"
      - "4571:4571"

    environment:
      - SERVICES=dynamodb,s3,sqs,sns  # lista de serviços que deseja subir 
      - DEBUG=1
      - DATA_DIR=/tmp/localstack/data
      - AWS_DEFAULT_REGION=us-east-1
      - HOST_TMP_FOLDER=${TMPDIR:-/tmp/}localstack
      - DOCKER_HOST=unix:///var/run/docker.sock
    volumes:
      - "./.localstack:/tmp/localstack"
      - "/var/run/docker.sock:/var/run/docker.sock"
```

## Variáveis de ambiente importantes   
SERVICES=dynamodb,s3,sqs,sns &rarr; lista de serviços que deseja subir   


## Verificar a saúde dos serviços da localstack   
http://localhost:4566/health


## Instalar o AWS CLI   
* Baixar o CLI 
  https://aws.amazon.com/pt/cli/   
  https://awscli.amazonaws.com/AWSCLIV2.pkg
  ou 
  brew install awscli

* Editar os arquivos de configuração  **~/.aws/config** com o seguinte conteúdo :   
   ```
    [default]
    region=us-east-1
    output=json
    ```
* Editar os arquivos de configuração  **~/.aws/credentials** com o seguinte conteúdo :   
   ```
    [default]
    aws_access_key_id=fake
    aws_secret_access_key=fake
    ```
**OBSERVAÇÃO:** utilize o editor _nano_ pois caso o arquivo não exista ele já cria   

## Para acessar o sns/sns  (duas maneiras)  
Utilizaremos o aws sns sqs client 
* git clone https://github.com/ajyounguk/aws-sqs-sns-client.git
* Criar o arquivo **./aws-sqs-sns-client/config/aws-config.json**  
  ```
  { 
    "accessKeyId": "test",
    "secretAccessKey": "test",
    "region": "us-east-1"
  } ```
* Criar o arquivo **./aws-sqs-sns-client/config/aws-override.json**  
 ```
  { 
    "sqs_endpoint": "http://localhost:4566",
    "sns_endpoint": "http://localhost:4566"
  }
  ```
* Executar ```node app.js```  
* Acessar  http://localhost:3000 

## Segunda maneira para acessar   
```
  sqs-sns-client:
    image: lucksjb/aws-sqs-sns-client
    container_name: sqs-sns-client
    ports:
      - "3000:3000"

    networks:
      - api-net
```

## Houve uma modificação no código do [aws-sqs-sqn-client](https://github.com/ajyounguk/aws-sqs-sns-client) para que o mesmo funcionasse em containers   
[Dockerfile](./Dockerfile)   
[aws-sqs-sqn-client source code modified](./aws-sqs-sns-client)   

## Comandos AWS-CLI
# Cria a fila com o nome FILA_DYNAMO
aws sqs create-queue --queue-name FILA_DYNAMO --endpoint-url=http://localhost:4566

# Cria a fila com o nome FILA_S3
aws sqs create-queue --queue-name FILA_S3 --endpoint-url=http://localhost:4566

# Cria o topico com o nome TOPICO_MENSAGENS
aws sns create-topic --name TOPICO_MENSAGENS --endpoint-url=http://localhost:4566

# Cria uma assinatura entre a fila FILA_DYNAMO e o topico TOPICO_MENSAGENS
aws sns subscribe --topic-arn arn:aws:sns:us-east-1:000000000000:TOPICO_MENSAGENS --protocol sqs --notification-endpoint arn:aws:sqs:us-east-1:000000000000:FILA_DYNAMO --endpoint-url=http://localhost:4566

# Cria uma assinatura entre a fila FILA_S3 e o topico TOPICO_MENSAGENS
aws sns subscribe --topic-arn arn:aws:sns:us-east-1:000000000000:TOPICO_MENSAGENS --protocol sqs --notification-endpoint arn:aws:sqs:us-east-1:000000000000:FILA_S3 --endpoint-url=http://localhost:4566

# Cria um bucket s3 com nome bucket-mensagens
aws s3api create-bucket --bucket bucket-mensagens --region us-east-1 --endpoint-url=http://localhost:4566

# Cria uma tabela no dynamodb com nome TabelaMensagens
aws dynamodb create-table --table-name TabelaMensagens --attribute-definitions AttributeName​=id,AttributeType=S --key-schema AttributeName​=id,KeyType=HASH --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 --endpoint-url=http://localhost:4566

## Links interessantes
https://www.linkedin.com/pulse/simulando-servi%25C3%25A7os-aws-na-sua-m%25C3%25A1quina-com-localstack-james-g-silva/?trackingId=xplfeNYXTc%2BVsIWCA434KQ%3D%3D

https://medium.com/trainingcenter/localstack-testando-servi%C3%A7os-aws-7f9f24de293c

https://github.com/localstack/ &rarr; repositório oficial localstack    
https://docs.localstack.cloud/ &rarr; página oficial localstack   

https://github.com/ajyounguk/aws-sqs-sns-client &rarr; repositório oficial do aws-sqs-sns cliente    



By    
    Luciano D. Silva - Luck    
    special tnx by Jamerson Silva
    