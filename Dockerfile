# to build : docker build -t lucksjb/aws-sqs-sns-client .
# to push : 
#    docker login 
# docker push lucksjb/aws-sqs-sns-client 

FROM node:14.18-alpine3.12

WORKDIR /usr/cli

COPY ./aws-sqs-sns-client .
# RUN     git clone https://github.com/ajyounguk/aws-sqs-sns-client && \
#         cd aws-sqs-sns-client && \
#         npm install 

ENTRYPOINT [ "node", "app.js" ]